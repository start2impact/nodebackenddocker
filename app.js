require('dotenv').config()
var express = require('express');
var logger = require('morgan');

var indexRouter = require('./routes/index');

var app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use('/', indexRouter);

const port = parseInt(process.env.PORT || 8080, 10)
app.listen(port, ()=>{
    console.log("Listening on http://localhost:"+port)
})
