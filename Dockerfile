FROM node:14

WORKDIR /var/www

COPY . ./

RUN yarn

CMD ["sh", "-c", "node app.js"]